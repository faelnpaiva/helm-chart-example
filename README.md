# Helm Chart Example

- helm dependency update
- helm install <name> -f values.yaml <path of chart> --namespace <name>
- helm delete <name> --namespace <name>
# Service

- Used helpers.tpl to support standards variables
- Type is NodePort because as ClusterIP healthcheck/livess wont work
- Changing target from container to k8s
# Ingress
- Using helpers.tpl to some variables
- annotations ( including to tls , but not enabled )
- Issuer and certificate configured 
# Deploy
- Using helpers.tpl

- Exampling some sintax of chart as :

1 - range to iterate in values.yaml
    
env:
    {{- range $pkey, $pval := .Values.variables }}
        - name: {{ $pkey }}
          value: {{ quote $pval }}
    {{- end }}


 
2- Getting a entire block from values.yaml 
 
 resources:
{{ toYaml .Values.resources  | indent 12 }}


- Exampling how to use rediness/liveness probes

- Exampling how to use a secret

- Exampling how to mount a volume

# Secrets

- Exampling how to create a secret ( must be base64)
- Improvements[WIP] : Inject secret using vault injector

# Helpers.tpl

- Named templates ( https://helm.sh/docs/chart_template_guide/named_templates/  )

# Chart.yaml
- It's a required file to create your chart . It's possible to set dependencies from other charts , version , chart name ,, etc ..
- Details in ~> https://helm.sh/docs/topics/charts/ 

# Values.yaml

- Here are the values used to populate your templates .